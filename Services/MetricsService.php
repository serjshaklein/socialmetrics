<?php
/**
 * Created by PhpStorm.
 * User: serj
 * Date: 3/20/18
 * Time: 7:17 PM
 */

namespace Services;


use Entity\Metric;

class MetricsService
{
    public function storeMetric(array $data): void
    {

        $metric = new Metric();

        foreach ($data as $property => $value) {
            $method = sprintf('set%s', ucwords($property));
            $method = str_replace("_", "", $method);
            $metric->$method($value);
        }
        /**
         * @todo Add validation
         */
        return $metric;


    }



}