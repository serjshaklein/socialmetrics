<?php
/**
 * Created by PhpStorm.
 * User: serj
 * Date: 3/20/18
 * Time: 7:44 PM
 */

namespace Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Source
 * @package Entity
 * @ORM\Table(name="source")
 * @ORM\Entity
 */

class Source
{


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="`type`", type="string", length=255)
     */

    private $type;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     */

    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="network")
     */
    private $network;

}