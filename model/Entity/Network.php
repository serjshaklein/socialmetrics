<?php
/**
 * Created by PhpStorm.
 * User: serj
 * Date: 3/20/18
 * Time: 7:49 PM
 */

namespace Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Network
 * @package Entity
 * @ORM\Table(name="network")
 * @ORM\Entity
 */

class Network
{


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     */

    private $title;
}