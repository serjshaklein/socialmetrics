<?php
/**
 * Created by PhpStorm.
 * User: serj
 * Date: 3/20/18
 * Time: 7:36 PM
 */

namespace Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Metric
 * @package Entity
 * @ORM\Table(name="metric")
 * @ORM\Entity
 */

class Metric
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="called_at", type="datetime")
     */
    private $calledAt;

    /**
     * @ORM\Column(name="request_time", type="integer")
     */
    private $requstTime;

    /**
     * @ORM\Column(name="posts_returned", type="integer")
     */
    private $postsReturned;

    /**
     * @ORM\ManyToOne(targetEntity="source")
     */
    private $source;

}
