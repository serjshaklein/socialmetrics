<?php
/**
 * Created by PhpStorm.
 * User: serj
 * Date: 3/20/18
 * Time: 7:20 PM
 */

namespace Controller;

use Doctrine\ORM\EntityManager;
use Services\MetricsService;

class IndexController
{
    public function storeAction(array $request, EntityManager $entityManager)
    {
        $metricService = new MetricsService();
        /**
         * @todo: process request properly
         */
        $data = $request['data'];
        try {
            $metric = $metricService->storeMetric($data);
            $entityManager->persist($metric);
            $entityManager->flush();


        } catch (\Exception $exception) {
            /**
             * @todo Process exception here
             */
        }
    }

}